# yii2-module/yii2-module-helper

Some common classes that are used throughout yii2-module libraries

A module to merge informations from different modules that gathers the same data.

![coverage](https://gitlab.com/yii2-module/yii2-module-helper/badges/main/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/yii2-module/yii2-module-helper/badges/main/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install yii2-magic/yii2-module-helper ^8`


## License

MIT (See [license file](LICENSE)).
