<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-module-helper library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Helper\Commands;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use PhpExtended\Logger\BasicConsoleLogger;
use PhpExtended\Logger\SilentMultipleLogger;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Throwable;
use yii\BaseYii;
use yii\console\Controller;
use yii\console\ExitCode;
use Yii2Extended\Yii2Log\Psr3ToYii2Logger;

/**
 * ExtendedController class file.
 * 
 * This controller add all generic console arguments that may be used into
 * console commands available to all console commands.
 * 
 * In particular, those options are made available to all commands:
 * - verbosity levels :
 * 		-vvv (very very verbose) (debug)
 * 		-vv (very verbose) (notice)
 * 		-v (verbose) (info)
 * 		-q (quiet) (errors only)
 * 		-qq (very quiet) (alerts only)
 * 		-qqq (very very quiet) (emergencies only)
 * - force action :
 * 		-f (force)
 * - time limit action :
 * 		-u (until date) (format YYYY-mm-dd HH:ii:ss)
 * - historisation actions :
 * 		-y (specific year)
 * 
 * This command also adds a `LoggerInterface` object available with the
 * `$this->getLogger()` method.
 * 
 * @author Anastaszor
 */
abstract class ExtendedController extends Controller
{
	use LoggerAwareTrait;
	
	/**
	 * Verbosity level 1 options.
	 *
	 * @var boolean
	 */
	public bool $verbose1 = false;
	
	/**
	 * Verbosity level 2 options.
	 *
	 * @var boolean
	 */
	public bool $verbose2 = false;
	
	/**
	 * Verbosity level 3 options.
	 *
	 * @var boolean
	 */
	public bool $verbose3 = false;
	
	/**
	 * Verbosity level 5 options.
	 *
	 * @var boolean
	 */
	public bool $quiet1 = false;
	
	/**
	 * Verbosity level 6 options.
	 *
	 * @var boolean
	 */
	public bool $quiet2 = false;
	
	/**
	 * Verbosity level 7 options.
	 *
	 * @var boolean
	 */
	public bool $quiet3 = false;
	
	/**
	 * Whether to force certain behavior.
	 * 
	 * @var boolean
	 */
	public bool $force = false;
	
	/**
	 * The year for when this command should be specified for.
	 * 
	 * @var ?integer
	 */
	public ?int $year = null;
	
	/**
	 * The time when this command started.
	 * 
	 * @var DateTimeImmutable
	 */
	protected DateTimeImmutable $fromTime;
	
	/**
	 * The time when this command should be stopped. Defaults is 22h later
	 * from start.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $untilTime = null;
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Controller::init()
	 */
	public function init() : void
	{
		parent::init();
		$this->fromTime = new DateTimeImmutable();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\console\Controller::options()
	 */
	public function options($actionID)
	{
		return \array_merge(parent::options($actionID), [
			'force',
			'quiet1', 'quiet2', 'quiet3',
			'untilTime',
			'verbose1', 'verbose2', 'verbose3',
			'year',
		]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\console\Controller::optionAliases()
	 * @return array<string, string>
	 * @psalm-suppress MixedReturnTypeCoercion
	 */
	public function optionAliases()
	{
		return \array_merge(parent::optionAliases(), [
			'f' => 'force',
			'q' => 'quiet1', 'qq' => 'quiet2', 'qqq' => 'quiet3',
			'u' => 'untilTime',
			'v' => 'verbose1', 'vv' => 'verbose2', 'vvv' => 'verbose3',
			'y' => 'year',
		]);
	}
	
	/**
	 * Gets the logger from the dynamical verbosity level.
	 * 
	 * @param boolean $renew
	 * @return LoggerInterface
	 */
	public function getLogger(bool $renew = false) : LoggerInterface
	{
		if($renew || null === $this->logger)
		{
			$basicConsoleLogger = new BasicConsoleLogger();
			if($this->quiet3)
			{
				$basicConsoleLogger->setVerbosityLevel(-3);
			}
			if($this->quiet2)
			{
				$basicConsoleLogger->setVerbosityLevel(-2);
			}
			if($this->quiet1)
			{
				$basicConsoleLogger->setVerbosityLevel(-1);
			}
			if($this->verbose1)
			{
				$basicConsoleLogger->setVerbosityLevel(1);
			}
			if($this->verbose2)
			{
				$basicConsoleLogger->setVerbosityLevel(2);
			}
			if($this->verbose3)
			{
				$basicConsoleLogger->setVerbosityLevel(3);
			}
			$psr3ToYii2Logger = new Psr3ToYii2Logger();
			$psr3ToYii2Logger->defaultLogCategory = __CLASS__;
			$this->logger = new SilentMultipleLogger([$basicConsoleLogger, $psr3ToYii2Logger]);
		}
		
		return $this->logger;
	}
	
	/**
	 * Gets the time when this process has started.
	 * 
	 * @return DateTimeInterface
	 */
	public function getFromTime() : DateTimeInterface
	{
		return $this->fromTime;
	}
	
	/**
	 * Sets the until datetime with the given datetime.
	 * 
	 * @param string $dateTime format YYYY-mm-dd HH:ii:ss
	 */
	public function setUntilTime(string $dateTime) : void
	{
		$dateTime = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $dateTime);
		if($dateTime instanceof DateTimeImmutable)
		{
			$this->untilTime = $dateTime;
		}
	}
	
	/**
	 * Gets the time when this process should be stopped (default is start+22h).
	 * 
	 * @return DateTimeInterface
	 */
	public function getUntilTime() : DateTimeInterface
	{
		if(null === $this->untilTime)
		{
			/** @psalm-suppress PossiblyFalseArgument */
			$this->untilTime = $this->fromTime->add(DateInterval::createFromDateString('+22 hours'));
		}
		
		return $this->untilTime;
	}
	
	/**
	 * Gets the year of work for this command.
	 * 
	 * @return integer
	 */
	public function getYear() : int
	{
		if(null === $this->year)
		{
			return (int) \date('Y');
		}
		
		return (int) $this->year;
	}
	
	/**
	 * Runs the callable within a friendly error catching block.
	 * 
	 * @param callable():int $callable
	 * @return integer the exit code
	 */
	public function runCallable(callable $callable) : int
	{
		$backtrace = \debug_backtrace(\DEBUG_BACKTRACE_IGNORE_ARGS, 2);
		$callingMethod = ($backtrace[1]['class'] ?? static::class).($backtrace[1]['type'] ?? '::').($backtrace[1]['function'] ?? __METHOD__);
		$this->stdout('Begin Process '.$callingMethod."\n");
		$ret = ExitCode::OK;
		$dti = new DateTimeImmutable();
		BaseYii::beginProfile($callingMethod, static::class);
		$exitCode = ExitCode::UNSPECIFIED_ERROR;
		
		try
		{
			$exitCode = $callable();
		}
		catch(Throwable $exc)
		{
			$message = '';
			$previous = $exc;
			
			do
			{
				$message .= $previous->getMessage()."\n\n".$previous->getTraceAsString()."\n\n\n\n";
				$previous = $previous->getPrevious();
			}
			while(null !== $previous);
			
			BaseYii::error($message, static::class);
			$this->stderr($message);
		}
		BaseYii::endProfile($callingMethod, static::class);
		$eti = new DateTimeImmutable();
		$diff = $eti->diff($dti);
		$this->stdout('Process Terminated in '.$diff->format('%a days, %h hours, %i minutes and %s seconds')."\n\n");
		
		return $exitCode | $ret;
	}
	
}
