<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-module-helper library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Helper\Components;

use InvalidArgumentException;
use RuntimeException;
use Stringable;
use Throwable;
use yii\db\ActiveRecord;

/**
 * ObjectCollector class file.
 * 
 * This class collects all the data about objects that should be saved into
 * a database. This object handles redundancies between objects but does not
 * handle integrity constraints between tables and classes, as those are
 * managed directly by the database and the save order of this collector.
 * 
 * @author Anastaszor
 */
class ObjectCollector implements Stringable
{
	
	/**
	 * The number of records on any class to trigger a save of all the known
	 * objects.
	 * 
	 * @var integer
	 */
	protected int $_saveChunk = 500;
	
	/**
	 * The number of records summed for all classes to trigger a save of all
	 * the known objects (memory management).
	 * 
	 * @var integer
	 */
	protected int $_saveGlobal = 10000;
	
	/**
	 * The current count of objects to save in the data array (cached).
	 * 
	 * @var integer
	 */
	protected int $_objectCount = 0;
	
	/**
	 * The object updater for this collector.
	 * 
	 * @var ObjectUpdater
	 */
	protected ObjectUpdater $_objectUpdater;
	
	/**
	 * The ordering to save the objects that are dependant to each other.
	 * 
	 * @var array<integer, class-string<ActiveRecord>>
	 */
	protected array $_saveOrder = [];
	
	/**
	 * The objects to save.
	 * 
	 * @var array<class-string<ActiveRecord>, array<string, ObjectRecord>>
	 */
	protected array $_data = [];
	
	/**
	 * Builds a new ObjectCollector with the given save order and save chunk.
	 * 
	 * @param ObjectUpdater $updater
	 * @param array<integer, class-string<ActiveRecord>> $saveOrder
	 * @param integer $saveChunk
	 * @param integer $saveGlobal
	 */
	public function __construct(ObjectUpdater $updater, array $saveOrder = [], int $saveChunk = 500, int $saveGlobal = 10000)
	{
		$this->_objectUpdater = $updater;
		$this->_saveOrder = $saveOrder;
		$this->_saveChunk = $saveChunk;
		$this->_saveGlobal = $saveGlobal;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Handles a given data as stream of objects to be saved deferred, such that
	 * every object given in the iterator can be decomposed in multiple savable
	 * objects.
	 * 
	 * @template T of ActiveRecord
	 * @param iterable<integer|string, T> $iterator
	 * @param array<integer, array{'class':class-string<ActiveRecord>, 'pkCallable':(callable(T):array<string,boolean|integer|float|string>), 'dataCallable':(callable(T):array<string,null|boolean|integer|float|string>)}> $classCallables
	 * @return integer the number of records saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function handleIteratorMultiple(iterable $iterator, array $classCallables) : int
	{
		$count = 0;
		
		foreach($iterator as $object)
		{
			foreach($classCallables as $classCallable)
			{
				$class = $classCallable['class'];
				$pkCallable = $classCallable['pkCallable'];
				$dataCallable = $classCallable['dataCallable'];
				$pks = $pkCallable($object);
				$data = $dataCallable($object);
				$count += $this->handleObjectClass($class, $pks, $data);
			}
		}
		
		return $count;
	}
	
	/**
	 * Handles a given data as sream of objects to be saved deferred.
	 * 
	 * @template T of ActiveRecord
	 * @param iterable<integer|string, T> $iterator
	 * @param class-string<T> $class
	 * @param callable(T):array<string,boolean|integer|float|string> $pkCallable
	 * @param callable(T):array<string,null|boolean|integer|float|string> $dataCallable
	 * @return integer the number of records saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function handleIteratorClass(iterable $iterator, string $class, callable $pkCallable, callable $dataCallable) : int
	{
		$count = 0;
		
		foreach($iterator as $object)
		{
			$pks = $pkCallable($object);
			$data = $dataCallable($object);
			$count += $this->handleObjectClass($class, $pks, $data);
		}
		
		return $count;
	}
	
	/**
	 * Handles a given data as an object to be saved deferred.
	 * 
	 * @param class-string<ActiveRecord> $class
	 * @param array<string, boolean|integer|float|string> $pks
	 * @param array<string, null|boolean|integer|float|string> $data
	 * @return integer the number of new records registered to be saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function handleObjectClass(string $class, array $pks, array $data) : int
	{
		$object = new ObjectRecord($class, $pks, $data);
		$hash = $object->getHash();
		if(isset($this->_data[$class][$hash]))
		{
			$this->_data[$class][$hash]->mergeWith($object);
			
			return 0;
		}
		
		$this->_data[$class][$hash] = $object;
		$this->_objectCount++;
		
		if($this->_objectCount > $this->_saveGlobal || \count($this->_data[$class]) > $this->_saveChunk)
		{
			$this->save();
			$this->_objectCount = 0;
			
			foreach($this->_data as $dataChunk)
			{
				$this->_objectCount += \count($dataChunk);
			}
		}
		
		return 1;
	}
	
	/**
	 * Saves all the known objects to the database. If the $saveOnlyClasses
	 * is provided,.
	 * 
	 * @param array<integer, class-string<ActiveRecord>> $saveOnlyClasses
	 * @return integer the number of records saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function save(array $saveOnlyClasses = []) : int
	{
		$saveOrder = empty($saveOnlyClasses) 
			? (empty($this->_saveOrder) ? \array_keys($this->_data) : $this->_saveOrder)
			: $saveOnlyClasses;
		
		$count = 0;
		
		foreach($saveOrder as $saveClass)
		{
			$count += $this->_objectUpdater->saveArrayEachRecord($saveClass, $this->_data[$saveClass] ?? []);
			
			unset($this->_data[$saveClass]);
		}
		
		return $count;
	}
	
	/**
	 * Destructor. Handles the save of the last known objects in this object.
	 */
	public function __destruct()
	{
		try
		{
			$this->save();
		}
		catch(Throwable $exc)
		{
			// nothing to do, must not throw
		}
	}
	
}
