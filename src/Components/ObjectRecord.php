<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-module-helper library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Helper\Components;

use Stringable;
use yii\db\ActiveRecord;

/**
 * ObjectRecord class file.
 * 
 * This class is a simple bean to store any data for any record object.
 * 
 * @author Anastaszor
 */
class ObjectRecord implements Stringable
{
	
	/**
	 * The class of the record to save.
	 * 
	 * @var class-string<ActiveRecord>
	 */
	protected string $_class;
	
	/**
	 * The hash that is the id of this object from its pk values.
	 * 
	 * @var string
	 */
	protected string $_hash;
	
	/**
	 * The primary keys of the record to save. (field => value).
	 * 
	 * @var array<string, boolean|integer|float|string>
	 */
	protected array $_pks = [];
	
	/**
	 * The data of the record to save. (field => value).
	 * 
	 * @var array<string, null|boolean|integer|float|string>
	 */
	protected array $_data = [];
	
	/**
	 * Builds a new ObjectRecord with the given pk and data.
	 * 
	 * @param class-string<ActiveRecord> $class
	 * @param array<string, boolean|integer|float|string> $pks
	 * @param array<string, null|boolean|integer|float|string> $data
	 */
	public function __construct(string $class, array $pks, array $data)
	{
		$this->_class = $class;
		$this->_pks = $pks;
		\ksort($this->_pks);
		$this->_data = $data;
		$this->_hash = \sha1(\implode('|', $this->_pks));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_class.'['.\implode('|', $this->_pks).']('.\implode(',', \array_keys($this->_data)).')';
	}
	
	/**
	 * Absorbs the given ObjectRecord if it's the same record class.
	 * 
	 * @param ObjectRecord $record
	 * @return boolean true if the merge is successful
	 */
	public function mergeWith(ObjectRecord $record) : bool
	{
		if($record->getClass() !== $this->getClass())
		{
			return false;
		}
		
		$this->_pks = \array_merge($this->_pks, $record->getPks());
		$this->_data = \array_merge($this->_data, $record->getData());
		$this->_hash = \sha1(\implode('|', $this->_pks));
		
		return true;
	}
	
	/**
	 * Gets the hash of this object.
	 * 
	 * @return string
	 */
	public function getHash() : string
	{
		return $this->_hash;
	}
	
	/**
	 * Gets the class of this record.
	 * 
	 * @return class-string<ActiveRecord>
	 */
	public function getClass() : string
	{
		return $this->_class;
	}
	
	/**
	 * Gets the primary keys of this record.
	 * 
	 * @return array<string, boolean|integer|float|string>
	 */
	public function getPks() : array
	{
		return $this->_pks;
	}
	
	/**
	 * Gets the data of this record.
	 * 
	 * @return array<string, null|boolean|integer|float|string>
	 */
	public function getData() : array
	{
		return $this->_data;
	}
	
}
