<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-module-helper library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Helper\Components;

use Stringable;

/**
 * DatatourismeQueryCounter class file.
 * 
 * This class is to count the queries that are made when an update is done.
 * 
 * @author Anastaszor
 */
class QueryCounter implements Stringable
{
	
	/**
	 * The selected counting.
	 * 
	 * @var array<class-string, integer>
	 */
	protected array $_selectedCount = [];
	
	/**
	 * The selected time used.
	 * 
	 * @var array<class-string, float>
	 */
	protected array $_selectedTime = [];
	
	/**
	 * The selected last time when registered.
	 * 
	 * @var array<class-string, float>
	 */
	protected array $_selectedLast = [];
	
	/**
	 * The saved counting.
	 * 
	 * @var array<class-string, integer>
	 */
	protected array $_savedCount = [];
	
	/**
	 * The saved time used.
	 * 
	 * @var array<class-string, float>
	 */
	protected array $_savedTime = [];
	
	/**
	 * The saved last time when registered.
	 * 
	 * @var array<class-string, float>
	 */
	protected array $_savedLast = [];
	
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Counts one selected query.
	 * 
	 * @param class-string $className
	 * @return QueryCounter
	 */
	public function updateSelected(string $className) : QueryCounter
	{
		$this->_selectedCount[$className] = ($this->_selectedCount[$className] ?? 0) + 1;
		$this->_savedLast[$className] = \microtime(true);
		
		return $this;
	}
	
	/**
	 * Halts the stopwatch for the given class name.
	 * 
	 * @param class-string $className
	 * @return QueryCounter
	 */
	public function watchSelected(string $className) : QueryCounter
	{
		if(isset($this->_selectedLast[$className]))
		{
			$time = \microtime(true) - $this->_selectedLast[$className];
			
			$this->_selectedTime[$className] = ($this->_selectedTime[$className] ?? 0.0) + $time;
		}
		
		return $this;
	}
	
	/**
	 * Counts one saved query.
	 * 
	 * @param class-string $className
	 * @return QueryCounter
	 */
	public function updateSaved(string $className) : QueryCounter
	{
		$this->_savedCount[$className] = ($this->_savedCount[$className] ?? 0) + 1;
		$this->_savedLast[$className] = \microtime(true);
		
		return $this;
	}
	
	/**
	 * Halts the stopwatch for the given class name.
	 * 
	 * @param class-string $className
	 * @return QueryCounter
	 */
	public function watchSaved(string $className) : QueryCounter
	{
		if(isset($this->_savedLast[$className]))
		{
			$time = \microtime(true) - $this->_savedLast[$className];
			
			$this->_savedTime[$className] = ($this->_savedTime[$className] ?? 0.0) + $time;
		}
		
		return $this;
	}
	
	/**
	 * Gets the select counts.
	 * 
	 * @return array<class-string, array{'count': integer, 'time': float}>
	 */
	public function getSelected() : array
	{
		\ksort($this->_selectedCount);
		
		$values = [];
		
		foreach($this->_selectedCount as $className => $count)
		{
			$values[$className] = ['count' => $count, 'time' => $this->_selectedTime[$className] ?? 0.0];
		}
		
		return $values;
	}
	
	/**
	 * Gets the saved counts.
	 * 
	 * @return array<class-string, array{'count': integer, 'time': float}>
	 */
	public function getSaved() : array
	{
		\ksort($this->_savedCount);
		
		$values = [];
		
		foreach($this->_savedCount as $className => $count)
		{
			$values[$className] = ['count' => $count, 'time' => $this->_savedTime[$className] ?? 0.0];
		}
		
		return $values;
	}
	
}
