<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-module-helper library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Helper\Components;

use ArrayIterator;
use InvalidArgumentException;
use Iterator;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use RuntimeException;
use Stringable;
use yii\db\ActiveRecord;

/**
 * ObjectUpdater class file.
 * 
 * This class updates raw data using the active record pattern given by
 * the yii framework.
 * 
 * @author Anastaszor
 */
class ObjectUpdater implements Stringable
{
	use LoggerAwareTrait;
	
	/**
	 * The batch size, meaning the number of records handled in a single action
	 * (querying, or inserting), when using the batch methods.
	 * 
	 * @var integer the batch size
	 */
	public static int $batchSize = 10000;
	
	/**
	 * Gets the query counter.
	 * 
	 * @var ?QueryCounter
	 */
	protected static ?QueryCounter $_queryCounter = null;
	
	/**
	 * Gets the query counter.
	 * 
	 * @return QueryCounter
	 */
	public static function getQueryCounter() : QueryCounter
	{
		if(null === static::$_queryCounter)
		{
			static::$_queryCounter = new QueryCounter();
		}
		
		return static::$_queryCounter;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets a suitable logger for this updater. By default, if no logger
	 * has been defined with the LoggerAwareTrait, a NullLogger is returned.
	 * 
	 * @return LoggerInterface
	 */
	public function getLogger() : LoggerInterface
	{
		if(null === $this->logger)
		{
			$this->logger = new NullLogger();
		}
		
		return $this->logger;
	}
	
	/**
	 * Updates the given iterator of objects to the given active record
	 * as specified with the classes and callables that returns the primary keys
	 * and the data from the source object.
	 * Multiple ActiveRecord can be used in the same array of class callables,
	 * with each callable retrieving the primary keys and data to build records
	 * of this class.
	 * 
	 * @template T of object
	 * @param iterable<integer|string, T> $iterator
	 * @param array<integer, array{'class':class-string<ActiveRecord>, 'pkCallable':(callable(T):array<string,boolean|integer|float|string>), 'dataCallable':(callable(T):array<string,null|boolean|integer|float|string>)}> $classCallables
	 * @return integer the number of records saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function saveIteratorEachMultiple(iterable $iterator, array $classCallables) : int
	{
		$itClass = \is_array($iterator) ? 'array' : \get_class($iterator);
		$rClasses = [];
		
		foreach($classCallables as $classCallable)
		{
			$rClasses[] = $classCallable['class'];
		}
		
		$this->getLogger()->info('Processing Iterator on Each {itClass} for : {rClasses}', ['itClass' => $itClass, 'rClasses' => \implode(', ', $rClasses)]);
		
		$count = 0;
		
		foreach($iterator as $object)
		{
			foreach($classCallables as $classCallable)
			{
				$class = $classCallable['class'];
				$pkCallable = $classCallable['pkCallable'];
				$dataCallable = $classCallable['dataCallable'];
				$pks = $pkCallable($object);
				$data = $dataCallable($object);
				$record = $this->saveObjectClass($class, $pks, $data);
				$count += (int) $record->getIsNewRecord();
			}
		}
		
		return $count;
	}
	
	/**
	 * Updates the given iterator of objects to the given active record as 
	 * specified with the class, and the callable to transform the object to
	 * the primary keys and to the data.
	 * 
	 * @template T of object
	 * @param iterable<integer|string, T> $iterator
	 * @param class-string<ActiveRecord> $class
	 * @param callable(T):array<string,boolean|integer|float|string> $pkCallable
	 * @param callable(T):array<string,null|boolean|integer|float|string> $dataCallable
	 * @return integer the number of records saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function saveIteratorEachClass(iterable $iterator, string $class, callable $pkCallable, callable $dataCallable) : int
	{
		$itClass = \is_array($iterator) ? 'array' : \get_class($iterator);
		$this->getLogger()->info('Processing Iterator on Each {itClass} for {rClass}', ['itClass' => $itClass, 'rClass' => $class]);
		
		$count = 0;
		
		/** @var T $object */
		foreach($iterator as $object)
		{
			$pks = $pkCallable($object);
			$data = $dataCallable($object);
			$object = $this->saveObjectClass($class, $pks, $data);
			$count += (int) $object->getIsNewRecord();
		}
		
		return $count;
	}
	
	/**
	 * Updates all the given data records one by one.
	 * 
	 * @param class-string<ActiveRecord> $class
	 * @param array<integer|string, ObjectRecord> $objectRecords
	 * @return integer the number of records saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function saveArrayEachRecord(string $class, array $objectRecords) : int
	{
		$this->getLogger()->info('Processing Array for {rclass}', ['rclass' => $class]);
		
		$count = 0;
		
		/** @var ObjectRecord $objectRecord */
		foreach($objectRecords as $objectRecord)
		{
			$object = $this->saveObjectClass($objectRecord->getClass(), $objectRecord->getPks(), $objectRecord->getData());
			$count += (int) $object->getIsNewRecord();
		}
		
		return $count;
	}
	
	/**
	 * Updates all the given data records one by one.
	 *
	 * @param class-string<ActiveRecord> $class
	 * @param Iterator<integer|string, ObjectRecord> $objectRecords
	 * @return integer the number of records saved
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function saveIteratorEachRecord(string $class, Iterator $objectRecords) : int
	{
		$this->getLogger()->info('Save Iterator Each Record for {rclass}', ['rclass' => $class]);
		
		$count = 0;
		
		/** @var ObjectRecord $objectRecord */
		foreach($objectRecords as $objectRecord)
		{
			$object = $this->saveObjectClass($objectRecord->getClass(), $objectRecord->getPks(), $objectRecord->getData());
			$count += (int) $object->getIsNewRecord();
		}
		
		return $count;
	}
	
	/**
	 * @template T of ActiveRecord 
	 * @param class-string<ActiveRecord> $class
	 * @param array $pks
	 * @param array $data
	 * @param bool $recursive
	 * @param Iterator<integer|string, ActiveRecord> $objectRecords
	 * @param class-string<T> $class
	 * @param Iterator<integer|string, T> $objectRecords
	 * @return integer the number of records saved
	 */
// 	public function saveIteratorBatchRecord(string $class, Iterator $objectRecords) : int
// 	{
// 		$this->getLogger()->info('Save Iterator Batch Record for {rclass}', ['rclass' => $class]);
		
// 		$updated = [];
// 		$inserted = [];
// 		$count = 0;
		
// 		/** @var ActiveRecord $objectRecord */
// 		foreach($objectRecords as $objectRecord)
// 		{
// 			$count++;
			
// 			if($objectRecord->getIsNewRecord())
// 			{
// 				$inserted[] = $objectRecord;
// 			}
// 			else
// 			{
// 				$updated[] = $objectRecord;
// 			}
			
// 			if(\count($updated) % self::$batchSize === 0)
// 			{
// 				$count += $this->saveIteratorEachRecord($class, new ArrayIterator($updated));
// 			}
			
// 			if(\count($inserted) % self::$batchSize === 0)
// 			{
// 				// $connection = $class::getDb();
// 				// $command = $connection->createCommand();
// 				// $command->batchInsert($class::tableName(), $columns, $rows);
// 				// TODO to be completed in batch insert
// 				$count += $this->saveIteratorEachRecord($class, new ArrayIterator($inserted));
// 			}
// 		}
		
// 		if(\count($updated) > 0)
// 		{
// 			$count += $this->saveIteratorEachRecord($class, new ArrayIterator($updated));
// 		}
		
// 		if(\count($inserted) > 0)
// 		{
// 			// $connection = $class::getDb();
// 			// $command = $connection->createCommand();
// 			// $command->batchInsert($class::tableName(), $columns, $rows);
// 			// TODO to be completed in batch insert
// 			$count += $this->saveIteratorEachRecord($class, new ArrayIterator($inserted));
// 		}
		
// 		return $count;
// 	}
	
	/**
	 * Updates the given data as object. The class of this object is used to
	 * retrieve the object model from the active record.
	 * 
	 * @template T of ActiveRecord 
	 * @param class-string<T> $class
	 * @param array<string, boolean|integer|float|string> $pks
	 * @param array<string, null|boolean|integer|float|string> $data
	 * @param boolean $recursive whether this function already called itself
	 * @return T the object saved in the db
	 * @throws InvalidArgumentException if a field is given but does not
	 *                                  exists in the class of the given object
	 * @throws InvalidArgumentException if the data does not validate
	 * @throws RuntimeException if the data cannot be saved
	 */
	public function saveObjectClass(string $class, array $pks, array $data, bool $recursive = false) : ActiveRecord
	{
		/** @var ?ActiveRecord $record */
		/** @var ?T $record */
		$record = $class::findOne($pks);
		
		if(null === $record)
		{
			/** @var ActiveRecord $record */
			/** @var T $record */
			/** @psalm-suppress UnsafeInstantiation */
			$record = new $class();
			$this->updateAttributes($record, $pks);
		}
		
		return $this->saveObject($record, $pks, $data, $recursive);
	}
	
	/**
	 * Updates the given data as object. The class of this object is used to
	 * retrieve the object model from the active record.
	 * 
	 * @template T of ActiveRecord
	 * @param T $record the object to save
	 * @param array<string, boolean|integer|float|string> $pks
	 * @param array<string, null|boolean|integer|float|string> $data
	 * @param boolean $recursive whether this function already called itself
	 * @return T the object saved in the db
	 * @throws InvalidArgumentException if a field is given but does not
	 *                                  exists in the class of the given object
	 * @throws InvalidArgumentException if the data does not validate
	 * @throws RuntimeException if the data cannot be saved
	 */
	public function saveObject(ActiveRecord $record, array $pks, array $data, bool $recursive = false) : ActiveRecord
	{
		// if no pks and data are given, save it anyway
		$hasChanged = $this->updateAttributes($record, $data) || $record->isNewRecord || (empty($pks) && empty($data));
		
		// if the record hasn't changed, we don't need to save it.
		if(!$hasChanged)
		{
			return $record;
		}
		
		$ids = [];
		
		foreach((array) $record->getPrimaryKey(true) as $kvalue)
		{
			$ids[] = (string) $kvalue;
		}
		
		$this->getLogger()->debug('Saving {class} {id}', ['class' => \get_class($record), '{id}' => \implode('|', $ids)]);
		
		$exception = null;
		
		try
		{
			static::getQueryCounter()->updateSaved(\get_class($record));
			$result = $record->save();
			static::getQueryCounter()->watchSaved(\get_class($record));
			
			if($result)
			{
				return $record;
			}
		}
		catch(\yii\db\Exception $exc)
		{
			static::getQueryCounter()->watchSaved(\get_class($record));
			
			if(!$recursive)
			{
				// MYSQL : catch concurrency access with duplicated record
				// then try to update it instead if it was an insert
				if(1062 === ((int) ($exc->errorInfo[0] ?? 0)) && $record->isNewRecord)
				{
					return $this->saveObjectClass(\get_class($record), $pks, $data, true);
				}
			}
			
			$exception = $exc;
		}
		
		$errors = [];
		
		foreach($record->getErrorSummary(true) as $error)
		{
			$errors[] = (string) $error;
		}
		
		$message = "Failed to save {class} : {errs}\nObject Current Data :\n{obj}\nObject Old Data :\n{old}";
		$context = [
			'{class}' => \get_class($record),
			'{errs}' => \implode(',', $errors),
			'{obj}' => \json_encode($record->getAttributes(), \JSON_PRETTY_PRINT),
			'{old}' => \json_encode($record->getOldAttributes(), \JSON_PRETTY_PRINT),
		];
		
		throw new InvalidArgumentException(\strtr($message, $context), -1, $exception);
	}
	
	/**
	 * Updates the values of the given active record object.
	 * 
	 * @param ActiveRecord $record
	 * @param array<string, null|boolean|integer|float|string> $attributes
	 * @return boolean true if the record has changed
	 * @throws InvalidArgumentException
	 */
	public function updateAttributes(ActiveRecord $record, array $attributes) : bool
	{
		$hasChanged = false;
		
		foreach($attributes as $dkey => $dvalue)
		{
			if(null === $dvalue)
			{
				continue;
			}
			$this->checkAttribute($record, $dkey);
			
			$oldVal = $record->getAttribute($dkey);
			if((string) $oldVal === (string) $dvalue)
			{
				// value already conform to wanted
				continue;
			}
			
			try
			{
				$record->setAttribute($dkey, $dvalue);
			}
			catch(\yii\base\InvalidArgumentException $exc)
			{
				throw new InvalidArgumentException('Invalid key '.$dkey, -1, $exc);
			}

			$hasChanged = true;
		}
		
		return $hasChanged;
	}
	
	/**
	 * Checks whether the given attribute exists in the given active record class.
	 * 
	 * @param ActiveRecord $record
	 * @param string $attributeName
	 * @return boolean true
	 * @throws InvalidArgumentException if the attribute does not exists in
	 *                                  the record class
	 */
	public function checkAttribute(ActiveRecord $record, string $attributeName) : bool
	{
		if(!$record->hasAttribute($attributeName))
		{
			$message = 'Failed to find attribute "{attr}" in record "{class}".';
			$context = ['{attr}' => $attributeName, '{class}' => \get_class($record)];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		return true;
	}
	
}
