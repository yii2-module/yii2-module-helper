<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-module-helper library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Helper;

use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use RuntimeException;
use yii\base\BootstrapInterface;
use yii\base\Module;
use yii\BaseYii;
use yii\helpers\Inflector;
use yii\i18n\PhpMessageSource;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Extended\Metadata\ModuleTrait;
use Yii2Extended\Yii2Log\Psr3ToYii2Logger;

/**
 * BootstrappedModule class file.
 * 
 * This class represents a basic Module with basic functionalities to have
 * 
 * @author Anastaszor
 */
class BootstrappedModule extends Module implements BootstrapInterface, ModuleInterface
{
	use LoggerAwareTrait, ModuleTrait;
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Module::getInstance()
	 * @return static
	 * @throws RuntimeException if the module cannot be loaded
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public static function getInstance() : BootstrappedModule
	{
		$instance = parent::getInstance();
		
		$classElements = \explode('\\', static::class);
		$moduleId = Inflector::camel2id(\str_replace('Module', '', \array_pop($classElements)));
		if(null === $instance)
		{
			$instance = BaseYii::$app->getModule($moduleId);
		}
		
		if($instance instanceof static)
		{
			return $instance;
		}
		
		$message = 'Failed to get {class} module from application, is it present in the config file with id "{mid}" ?';
		$context = ['{class}' => static::class, '{mid}' => $moduleId];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\BootstrapInterface::bootstrap()
	 */
	public function bootstrap($app) : void
	{
		$classParts = \explode('\\', static::class);
		\array_pop($classParts);
		$namespace = \implode('\\', $classParts);
		/** @var \yii\base\Application $app */
		if($app instanceof \yii\console\Application)
		{
			$this->controllerNamespace = $namespace.'\\Commands';
			$rClass = new ReflectionClass($this);
			$this->controllerPath = \dirname((string) $rClass->getFileName()).\DIRECTORY_SEPARATOR.'Commands';
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see\Module::init
	 * @throws \yii\base\InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function init() : void
	{
		parent::init();
		
		if(null !== BaseYii::$app)
		{
			$classParts = \explode('\\', static::class);
			\array_pop($classParts);
			$namespace = \implode('\\', $classParts);
			$rClass = new ReflectionClass($this);
			$className = $rClass->getShortName();
			$fileDir = \dirname((string) $rClass->getFileName());
			
			if(BaseYii::$app instanceof \yii\web\Application)
			{
				$this->controllerNamespace = $namespace.'\\Controllers';
				$this->controllerPath = $fileDir.\DIRECTORY_SEPARATOR.'Controllers';
			}
			
			BaseYii::$app->getI18n()->translations[$className.'.*'] = [
				'class' => PhpMessageSource::class,
				'sourceLanguage' => 'en-US',
				'basePath' => $fileDir.\DIRECTORY_SEPARATOR.'Messages',
			];
		}
	}
	
	/**
	 * Gets a suitable logger for this module.
	 * 
	 * @return LoggerInterface
	 */
	public function getLogger() : LoggerInterface
	{
		if(null === $this->logger)
		{
			$logger = new Psr3ToYii2Logger();
			$logger->defaultLogCategory = __CLASS__;
			$this->logger = $logger;
		}
		
		return $this->logger;
	}
	
	/**
	 * Gets the path for the runtime of this module. This should not be used,
	 * and self::getAssetsDir(), self::getDataDir(), and self::getTempDir()
	 * should be used instead.
	 *
	 * @return string the path where all this module-specific data is stored
	 * @throws RuntimeException if the directory does not exists and cannot be created
	 */
	public function getRuntimeDir() : string
	{
		try
		{
			$basePath = (string) BaseYii::getAlias('@app/runtime');
		}
		catch(\yii\base\InvalidArgumentException $exc)
		{
			$message = 'Failed to resolve alias {ctx}. Please make sure that the application bootstraps this alias and that it is not removed.';
			$context = ['{ctx}' => '@app/runtime'];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
		
		if(!\is_dir($basePath))
		{
			$message = 'Failed to find runtime path from alias {ctx}. Please make sure that this directory exists and is writable.';
			$context = ['{ctx}' => '@app/runtime'];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		if(!\is_writable($basePath))
		{
			if(!@\chmod($basePath, 0775))
			{
				$message = 'Failed to have runtime path {path} writable. Please make sure that this directory is writable.';
				$context = ['{ctx}' => $basePath];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		
		$modulePath = $basePath.\DIRECTORY_SEPARATOR.\str_replace('/', '-', $this->getUniqueId());
		if(!\is_dir($modulePath))
		{
			if(!@\mkdir($modulePath, 0775))
			{
				$message = 'Failed to create module runtime path at {path}. Please make sure that the application runtime directory at {adir} exists and is writable.';
				$context = ['{path}' => $modulePath, '{adir}' => $basePath];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		
		if(!\is_writable($modulePath))
		{
			if(!@\chmod($modulePath, 0775))
			{
				$message = 'Failed to have module runtime path at {path} writable. Please make sure that this directory is writable.';
				$context = ['{path}' => $modulePath];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		
		return $modulePath;
	}
	
	/**
	 * Gets the runtime directory for this module dedicated to assets data.
	 * 
	 * @return string the path where assets should be stored
	 * @throws RuntimeException if the directory does not exists and cannot be created
	 */
	public function getAssetsDir() : string
	{
		$assetsPath = $this->getRuntimeDir().\DIRECTORY_SEPARATOR.'assets';
		if(!\is_dir($assetsPath))
		{
			if(!@\mkdir($assetsPath, 0775))
			{
				$message = 'Failed to create module assets directory at {path}. Please make sure that the module runtime directory at {mdir} exists and is writable.';
				$context = ['{path}' => $assetsPath, '{mdir}' => $this->getRuntimeDir()];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		
		if(!\is_writable($assetsPath))
		{
			if(!@\chmod($assetsPath, 0775))
			{
				$message = 'Failed to have module assets directory at {path} writable. Please make sure that this directory is writable.';
				$context = ['{path}' => $assetsPath];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		
		return $assetsPath;
	}
	
	/**
	 * Gets the runtime directory for this module dedicated to plain/raw data.
	 * 
	 * @return string the path where raw data should be stored
	 * @throws RuntimeException if the directory does not exists and cannot be created
	 */
	public function getDataDir() : string
	{
		$dataPath = $this->getRuntimeDir().\DIRECTORY_SEPARATOR.'data';
		if(!\is_dir($dataPath))
		{
			if(!@\mkdir($dataPath, 0775))
			{
				$message = 'Failed to create module data directory at {path}. Please make sure that the module runtime directory at {mdir} exists and is writable.';
				$context = ['{path}' => $dataPath, '{mdir}' => $this->getRuntimeDir()];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		
		if(!\is_writable($dataPath))
		{
			if(!@\chmod($dataPath, 0775))
			{
				$message = 'Failed to have module data directory at {path} writable. Please make sure that this directory is writable.';
				$context = ['{path}' => $dataPath];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		
		return $dataPath;
	}
	
	/**
	 * Gets the runtime directory for this module dedicated to temporary data.
	 * 
	 * @return string the path where temp data should be stored
	 * @throws RuntimeException if the directory does not exists and cannot be created
	 */
	public function getTempDir() : string
	{
		$tempPath = $this->getRuntimeDir().\DIRECTORY_SEPARATOR.'temp';
		if(!\is_dir($tempPath))
		{
			if(!@\mkdir($tempPath, 0775))
			{
				$message = 'Failed to create module temp directory at {path}. Please make sure that the module runtime directory at {mdir} exists and is writable.';
				$context = ['{path}' => $tempPath, '{mdir}' => $this->getRuntimeDir()];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		
		if(!\is_writable($tempPath))
		{
			if(!@\chmod($tempPath, 0775))
			{
				$message = 'Failed to have module temp directory at {path} writable. Please make sure that this directory is writable.';
				$context = ['{path}' => $tempPath];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		
		return $tempPath;
	}
	
}
